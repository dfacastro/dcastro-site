function fixArticles() {

	var headerHeight = $("#header").height();
	var windowHeight = $(window).height();
	var bodyHeight = windowHeight - headerHeight;
	var tallScreen = true;

	$("article").each( function() {

		$(this).css('margin-top', 0);
		$(this).css('margin-bottom', 0);

		var postHeight = $(this).height();
				
		if (postHeight < bodyHeight) {
			$(this).css('padding-top', (bodyHeight-postHeight)/2 + headerHeight);
			$(this).css('padding-bottom', (bodyHeight-postHeight)/2 + headerHeight);
		} else {
			$(this).css('padding-top', headerHeight);
			tallScreen = false;
		}
	});

	// remove scroll bar
	if(tallScreen) {
		document.body.style.overflowY = 'hidden';
	}
	else {
		document.body.style.overflowY = 'visible';
	}

};

function isNarrow() {
	return $(window).width() < 800;
}

$(window).load(function() {
	fixArticles();
})

$(document).ready(function() {

	//save last article navigated to
	var lastArticle = '#' + $('article').first().attr('id');

	$("a.anchorLink").click(function() {		
		//if not animated
		if ( $("html:not(:animated),body:not(:animated)").length ) {
			lastArticle = $(this).attr('href');
		}
	});

	//hide stuff after 'read more' and display the Read More button
	$('.button.readmore').css('display', 'inline');

	$('.button.readmore').nextAll().wrapAll('<div class="readmore-content"/>').parent().each(function() {
		$(this).css('display', 'none');
	});

	//listener to handle the menu for mobile
	$('div#menu-button, nav#main-nav a').click(
		toggleMenu
	);

	//fix height of articles
	if( $(window).width() >= 800 || contentFits()) {
		fixArticles();
	}

	$(window).resize( function() {
		var fits = contentFits();

		if(fits) {
			document.body.style.overflowY = 'hidden';
			$.scrollTo(lastArticle);
		}
		else
			document.body.style.overflowY = 'visible';

		//if the window is narrow and can no longer hold the content
		if( ($(window).width() < 800) && (! fits)) {
			//console.log("disable");
			disableFixedArticles();
		}
		else {
			fixArticles();
		}
	});


	// functions
	function toggleMenu() {
		var page = $('html');
		var isNarrow = $(window).width() < 800;
		if (page.hasClass('menu-open') && isNarrow) {
			page.removeClass('menu-open');
			
		}
		else if (isNarrow) {
			page.addClass('menu-open');
		}
	};

	function disableFixedArticles() {
		var headerHeight = $("#header").height();

		$('article').css('padding-top', headerHeight );
		$('article').css('padding-bottom', 0 );
		$('article:last-of-type').css('padding-bottom', headerHeight );
	}
	
	function fixArticles() {

		var headerHeight = $("#header").height();
		var windowHeight = $(window).height();
		var bodyHeight = windowHeight - headerHeight;
		var tallScreen = true;

		$("article").each( function() {

			$(this).css('margin-top', 0);
			$(this).css('margin-bottom', 0);

			var postHeight = $(this).height();
					
			if (postHeight < bodyHeight) {
				$(this).css('padding-top', (bodyHeight-postHeight)/2 + headerHeight);
				$(this).css('padding-bottom', (bodyHeight-postHeight)/2 + headerHeight);
			} else {
				$(this).css('padding-top', headerHeight);
				tallScreen = false;
			}
		});

		// remove scroll bar
		if(tallScreen) {
			document.body.style.overflowY = 'hidden';
		}
		else {
			document.body.style.overflowY = 'visible';
		}
	
	};

	function contentFits() {
		var headerHeight = $("#header").height();
		var windowHeight = $(window).height();
		var bodyHeight = windowHeight - headerHeight;
		var tallScreen = true;

		$("article").each( function() {

			var postHeight = $(this).height();
					
			if (postHeight >= bodyHeight) {
				tallScreen = false;
			}
		});

		return tallScreen;
	}


	$(".readmore").click( function() {
		var div = $(this).next();
	
		if ( div.is(":hidden")) {
			//get div height
			div.css({'visibility':'hidden','display':'block'});
			var divHeight = div.height();
			div.css({'visibility':'visible','display':'none'});
			
			//show div
			div.slideDown('fast');

			//scroll down
			if(contentFits()) {
				$('html,body').animate({
					scrollTop: $(window).scrollTop() + (divHeight/2)
				}, 500);
			}
			$(this).parents("article").each( fixArticle);
	
		} else {
			var divHeight = div.height();
			
			//hide div
			div.slideUp('fast');

			//scroll up
			if(contentFits()) {
				$('html,body').animate({
					scrollTop: $(window).scrollTop() - (divHeight/2)
				}, 500);
			}
		}
	});
	
	
	$("nav#main-nav a").click(function() {
		$(".readmore").next().each(function() {
			if(! $(this).is(":hidden")) {
				$(this).hide();
			}
		});
	});

	/*
	$('div.album img').live('click', function(evt) {  // my code follows here })
		evt.stopPropagation();

		var children = $(this).closest('.album').find('div[data-picture]');
		if (children.length === 1)
			return;

		var next = $(this).parent().removeClass('open').next(); //.addClass('open');
		
		if(next.length === 0) {
			children.first().addClass('open');
		}
		else {
			next.addClass('open');
		}
	});


	$('.album-cover-container').click(function() {
		$(this).next('.album').addClass('open').find('div[data-picture]').first().addClass('open');

	});

	$('div.album').click(function(evt) {
		if (! $(evt.target).is('img'))
			$(this).removeClass('open').find('div[data-picture]').removeClass('open');

	});

	*/



 });