down_x = null
up_x = null

$ ->
	#$('html').addClass 'touch'

	$('body').append '''
		<div id="gallery">
			<div id="container">
				<div id="holder">
					<span>✘</span>
					<div data-picture/>
				</div>
			</div>
		</div>
	'''

	# add album-cover mask
	$('html:not(.touch) .album-cover-container').append('''
		<div class="mask">
			<h1>
				Press to view slideshow
			</h1>
			<p>
				Use the controls on the right/left to navigate
			</p>
		</div>
	''').wrapInner '<div/>'

	addControls()

	### slide right/left with mouse events
	$('#holder').bind 'mousedown', (e) ->
		e.preventDefault()
		down_x = e.pageX

	$('#holder').bind 'mouseup', (e) ->
		up_x = e.pageX;
		console.log('moved')
		slideToMove()
	###

	# slide right/left to switch pictures (touch events)
	$('#holder').bind 'touchstart', (e) ->
		down_x = e.originalEvent.touches[0].pageX

	$('#holder').bind 'touchmove', (e) ->
		e.preventDefault()
		up_x = e.originalEvent.touches[0].pageX

	$('#holder').bind 'touchend', (e) ->
		console.log 'touch'
		slideToMove()

	# open gallery
	$('.album-cover-container').click ->
		album = $(this).data('album')
		display(album, 1)
		$('#gallery').addClass 'open'
		$('#gallery').data 'current', album: album, picture: 1, count: $(this).data 'count'

	#close gallery
	$('#gallery').click ->
		$(this).removeClass 'open'

	#move onto next picture
	$('#gallery #holder').delegate 'div[data-picture] img', 'click', (evt) ->
		evt.stopPropagation()
		#moveToNext()

	$('#gallery #holder').delegate '> div:last-child', 'click', (evt) ->
		evt.stopPropagation()
		moveToNext()

	#move onto previous picture
	$('#gallery #holder').delegate '> div:first-child', 'click', (evt) ->
		evt.stopPropagation()
		moveToPrevious()


slideToMove = () ->
	if down_x - up_x > 70 then moveToNext()
	if up_x - down_x > 70 then moveToPrevious()

moveToNext = () ->
	current = $('#gallery').data 'current'
	if current.count != 1
		if current.count != current.picture
			display(current.album, ++current.picture)
		else
			display(current.album, current.picture = 1)

moveToPrevious = () ->
	current = $('#gallery').data 'current'
	if current.count != 1
		if 1 != current.picture
			display(current.album, --current.picture)
		else
			display(current.album, current.picture = current.count)

addControls = () ->
	if $('html').hasClass 'touch' then return
	
	holder = $('#gallery #holder')
	extension = if $('html').hasClass('svg') then 'svg' else 'png'

	if holder.children().length < 3
		holder.prepend '''
			<div class="previous-container">
				<div class="''' + extension + '''-container previous">
					<img src="assets/previous.''' + extension + '''" />
				</div>
			</div>
		'''

		holder.append '''
		    <div class="next-container">
		    	<div class="''' + extension + '''-container next">
		    		<img src="assets/next.''' + extension + '''" />
		    	</div>
		    </div>
		'''

display = (album, number) ->
	path = "assets/" + album + "/" + number

	$('#holder').html '''
			<span>✘</span>
			<div data-picture>
		        <div data-src="''' + path + '''thumb.png"></div>
		        <div data-src="''' + path + '''low.png" data-media="(min-width: 400px)"></div>
		        <div data-src="''' + path + '''high.png" data-media="(min-width: 800px)"></div>

		        <!--[if (lt IE 9) & (!IEMobile)]>
		            <div data-src="''' + path + '''low.png"></div>
		        <![endif]-->

		        <!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
		        <noscript>
		            <img src="''' + path + '''low.jpg">
		        </noscript>
		    </div>
	'''
	addControls()
	picturefill()